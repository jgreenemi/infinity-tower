import org.lwjgl.*;
import org.lwjgl.glfw.*;
import org.lwjgl.opengl.*;

import java.awt.*;
import java.io.File;
import java.io.IOException;

import static org.lwjgl.glfw.Callbacks.*;
import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.system.MemoryUtil.*;

import digiturtle.GLFont;

public class HelloWorld {

    /*
    *
    * Edit your VM Options for the run configuration to use the following if having trouble on a Mac:
    * -Djava.library.path=src/main/resources/libs -Dorg.lwjgl.util.Debug=false -Djava.awt.headless=true -XstartOnFirstThread
    *
     */
    // The window handle
    private long window;

    public void run() {
        System.out.println("Hello LWJGL " + Version.getVersion() + "!");

        try {
            init();
            loop();

            // Free the window callbacks and destroy the window
            glfwFreeCallbacks(window);
            glfwDestroyWindow(window);
        } finally {
            // Terminate GLFW and free the error callback
            glfwTerminate();
            glfwSetErrorCallback(null).free();
        }
    }

    private void init() {

        // Setup an error callback. The default implementation
        // will print the error message in System.err.
        GLFWErrorCallback.createPrint(System.err).set();

        // Initialize GLFW. Most GLFW functions will not work before doing this.
        if ( !glfwInit() )
            throw new IllegalStateException("Unable to initialize GLFW");

        // Configure our window
        glfwDefaultWindowHints(); // optional, the current window hints are already the default
        glfwWindowHint(GLFW_VISIBLE, GLFW_FALSE); // the window will stay hidden after creation
        glfwWindowHint(GLFW_RESIZABLE, GLFW_TRUE); // the window will be resizable

        int WIDTH = 250;
        int HEIGHT = 100;

        // Create the window
        window = glfwCreateWindow(WIDTH, HEIGHT, "Hello World!", NULL, NULL);
        if ( window == NULL )
            throw new RuntimeException("Failed to create the GLFW window");

        // Setup a key callback. It will be called every time a key is pressed, repeated or released.
        glfwSetKeyCallback(window, (window, key, scancode, action, mods) -> {
            if ( key == GLFW_KEY_ESCAPE && action == GLFW_RELEASE )
                glfwSetWindowShouldClose(window, true); // We will detect this in our rendering loop
        });

        // Get the resolution of the primary monitor
        GLFWVidMode vidmode = glfwGetVideoMode(glfwGetPrimaryMonitor());
        // Center our window
        glfwSetWindowPos(
                window,
                (vidmode.width() - WIDTH) / 2,
                (vidmode.height() - HEIGHT) / 2
        );

        // Make the OpenGL context current
        glfwMakeContextCurrent(window);
        // Enable v-sync
        glfwSwapInterval(1);

        // Make the window visible
        glfwShowWindow(window);

        try {
            System.out.println("Now loading GraphicsEnvironment...");
            Font customfont = Font.createFont(Font.TRUETYPE_FONT, new File("src/main/resources/LiberationSans-Regular.ttf"));
            GraphicsEnvironment.getLocalGraphicsEnvironment().registerFont(customfont);
            System.out.println("GraphicsEnvironment loaded, font registered!");
        } catch (IOException|FontFormatException e) {
            System.out.println("Error: " + e);
        }
    }

    private void loop() {


        //http://www.java-gaming.org/index.php?topic=35535.0
        // This is how we'll implement text rendering, using the digiturtle UI package.

        // This line is critical for LWJGL's interoperation with GLFW's
        // OpenGL context, or any context that is managed externally.
        // LWJGL detects the context that is current in the current thread,
        // creates the GLCapabilities instance and makes the OpenGL
        // bindings available for use.
        GL.createCapabilities();

        GLFont font;
        // Not entirely sure why, but the font seems to have to be created after the GL.createCapabilities() is called.
        // Currently, the font gets created but the window is not rendered properly (shows up black, never updates colour). Not sure why this occurs.
        System.out.println("Now creating font...");
        int style = Font.PLAIN;
        Font awtFont = new Font("Times New Roman",style,24);
        font = new GLFont(24,awtFont);
        System.out.println("Font created! Now make the window.");

        // Set the clear color
        glClearColor(1.0f, 0.0f, 0.0f, 0.0f);

        // Run the rendering loop until the user has attempted to close
        // the window or has pressed the ESCAPE key.
        while ( !glfwWindowShouldClose(window) ) {
            glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // clear the framebuffer
            glfwSwapBuffers(window); // swap the color buffers

            font.drawText(0,0,"If you can read this, things are working!", Color.blue); // This prints text to the window.

            // Poll for window events. The key callback above will only be
            // invoked during this call.
            glfwPollEvents();
       }
    }

    public static void main(String[] args) {
        new HelloWorld().run();
    }

}