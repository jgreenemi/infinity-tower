package digiturtle;

public interface Renderable {
	
	public void render();

}
