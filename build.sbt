name := "Infinity-Tower"

version := "1.0.2"

scalaVersion := "2.11.8"

libraryDependencies ++= Seq(
  "org.lwjgl" % "lwjgl" % "3.0.0"
)